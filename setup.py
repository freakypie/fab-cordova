#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='fab-cordova',
    version='0.1',
    description="A collection of fabric utils for cordova apps",
    author="Danemco, LLC",
    author_email='dev@velocitywebworks.com',
    url='https://bitbucket.org/freakypie/fab-cordova',
    packages=find_packages(),
    install_requires=['fabric>=1.7.0', 'requests', 'python-gitlab'],
)
