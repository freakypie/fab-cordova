from __future__ import print_function
from xml.dom import minidom
import json
import logging
from easy_thumbnails.files import get_thumbnailer
import os
import re
import requests
import shutil

from fabric.api import *  # @UnusedWildImport
from fabric.operations import local
from ._apps import apps, Key, CordovaApp, PhoneGapBuildAccount

try:
    from PIL import Image
except ImportError:
    import Image


class Plugin(object):
    path = None
    name = None  # dotted identifier
    platforms = []

    def __init__(self, name, path, platforms=["android", "ios"]):
        self.name = name
        self.path = path
        self.platforms = platforms

    def __str__(self):
        return "plugin: %s %s %s" % (self.name, self.path, self.platforms)

    def refresh(self, app):
        with settings(warn_only=True):
            self.uninstall(app)
        self.install(app)

    def install(self, app):
        raise NotImplemented()

    def uninstall(self, app):
        raise NotImplemented()


class PlugmanPlugin(Plugin):

    def install(self, app):
        for platform in self.available_platforms():
            local("""plugman install \
    --plugin {name} \
    --platform {platform} \
    --project platforms/{platform}/
            """.format(
                name=self.path or self.name,
                platform=platform
            ))

    def uninstall(self, app):
        with settings(warn_only=True):
            for platform in self.available_platforms():
                local("""plugman uninstall \
        --plugin {name} \
        --platform {platform} \
        --project platforms/{platform}/
                """.format(
                    name=self.name,
                    platform=platform
                ))


class CordovaPlugin(Plugin):

    def install(self, app):
        path = self.path
        if path and path.startswith("http"):
            filepath = os.path.abspath(os.path.join(".plugin-cache", self.name))
            if not os.path.isdir(filepath):
                try:
                    os.makedirs(filepath)
                except OSError:
                    pass
                local("git clone {url} {file}".format(url=path, file=filepath))
            path = filepath

        with lcd(app.location):
            local("cordova plugin add %s" % (path or self.name))

    def uninstall(self, app):
        with lcd(app.location):
            local("cordova plugin remove %s" % self.name)


class PluginManager(object):
    plugin_type = CordovaPlugin

    def __init__(self, app):
        self.app = app
        self.slug = app.slug
        self.location = app.location
        self.load()

    def path(self):
        return os.path.join(self.location)

    def filename(self):
        return os.path.join(self.path(), "config.xml")

    def load(self):
        try:
            self._plugins = {}
            xml = minidom.parse(self.filename())
            for row in xml.getElementsByTagName("gap:plugin"):
                name = row.attributes['name'].value
                try:
                    path = row.attributes['path'].value
                except KeyError:
                    path = None
                self._plugins[name] = self.plugin_type(name=name, path=path)
        except IOError:
            self._plugins = {}
            self.save()

    def save(self):
        with open(self.filename(), "w") as fileh:
            fileh.write(json.dumps(
                [p.__dict__ for p in self._plugins.values()], indent=4))

    def add(self, name, path=None, platform="android"):
        plugin = self._plugins.get(name)

        if plugin:
            if platform not in plugin.platforms:
                plugin.platforms.append(platform)
            if path:
                plugin.path = path
        else:
            plugin = Plugin(name=name, platforms=[platform], path=path)

        with lcd(self.path()):
            plugin.install(self.app)

        self._plugins[name] = plugin
        self.save()

    def check_install(self):
        """ checks each platform, if not installed, install """
        with lcd(self.path()):
            result = local("""cordova plugins ls""", capture=True)
            result = json.loads(result.replace("'", '"'))
            for plugin in self._plugins.values():
                if plugin.name not in result:
                    local("cordova plugin add %s" % (plugin.path or plugin.name))

    def refresh_all(self):
        for plugin in self._plugins.values():
            plugin.refresh(self.app)

    def get(self, name):
        return self._plugins.get(name, None)

    def all(self):
        return self._plugins.values()


@task(default=True)
def cordova(*commands):
    for app in apps:
        with lcd(app.location):
            for command in commands:
                local("cordova " + command)


@task
def use_plugman():
    PluginManager.plugin_type = PlugmanPlugin


@task
def use_cordova_plugins():
    PluginManager.plugin_type = CordovaPlugin


@task
def install_plugin(name, path=None, platform="android"):
    for app in apps:
        plugins = PluginManager(app)
        plugins.add(name, path=path, platform=platform)


@task
def list_plugins():
    for app in apps:
        with lcd(app.location):
            print(app.name)
            print(json.loads(local("cordova plugins ls", capture=True).replace("'", '"')))
            print()


@task
def killstart():
    for app in apps:
        with lcd(app.location):
            # cordova can't handle plugins well
            # i've found that if you don't reload the plugins manually
            # they will have errors loading on the device
            # local("rm -rf plugins/*.*")

            for platform in app.platforms:
                if platform.strip():
                    local("cordova platform remove " + platform)
                    local("rm -rf platforms/" + platform)
                    local("cordova platform add " + platform)

    execute("cordova.reload_plugins")


@task
def clear_plugins():
    for app in apps:
        with lcd(app.location):
            plugins = PluginManager(app)
            for plugin in plugins.all():
                plugin.uninstall()


@task
def apply_plugins():
    for app in apps:
        with lcd(app.location):
            plugins = PluginManager(app)
            for plugin in plugins.all():
                plugin.install()


@task
def reload_plugins():
    env.warn_only = True
    for app in apps:
#         with lcd(app.location):
#             local("rm -rf plugins/*.*")
        plugins = PluginManager(app)
        plugins.refresh_all()
    env.warn_only = False


@task
def release():
    apps.debug = False


@task
def select(*selected):
    apps.select(*selected)


@task
def android():
    print("*** Doing android only")
    apps.platforms = ["android"]


@task
def ios():
    print("*** Doing ios only")
    apps.platforms = ["ios"]


def resize(src, dest, width, height):
    if os.path.exists(src):
        print("Thumbnailing %s" % src)
        print("...", dest, (width, height))

        with open(src) as image_file:
            img = get_thumbnailer(image_file, os.path.basename(src))\
                .generate_thumbnail(dict(size=(width, height), crop="smart")).image

            try:
                os.makedirs(os.path.dirname(dest))
            except OSError:
                pass

            img.save(os.path.abspath(dest))
    else:
        print("source is missing! ({})".format(src))


@task
def debug():
    apps.debug = True


@task
def prepare():
    for app in apps:
        print(app, app.platforms)
        for platform in app.platforms:
            with lcd(app.location):
                local("cordova prepare --platform %s" % platform)

            if platform == "ios":
                #with settings(warn_only=True):
                #    local("cp 'patches/ios/MainViewController.m' '{location}/platforms/ios/{capped}/Classes/'".format(location=app.location, capped=app.name))

                filename = os.path.join(app.location, 'platforms/ios/{capped}/{capped}-Info.plist').format(capped=app.name)
                with open(filename, "r") as fileh:
                    content = fileh.read()
                    content = re.sub(
                        "<key>CFBundleVersion<\/key>\n?.*?<string>[^<]+<\/string>",
                        "<key>CFBundleVersion</key>\n<string>{code}</string>".format(code=app.build_code),
                        content
                    )
                    with open(filename, "w") as filew:
                        filew.write(content)

            if platform == "android":
                print("patching version code")

                filename = os.path.join(app.location, "platforms/android/AndroidManifest.xml")
                with open(filename, "r") as filer:
                    content = filer.read()

                    content = re.sub('android:versionCode="(\d+)"',
                        'android:versionCode="%s"' % app.build_code, content)
                    with open(filename, "w") as filew:
                        filew.write(content)

            print(
                "{app} on {platform} version patched to {code}".format(
                    app=app.name,
                    platform=platform,
                    code=app.build_code
                )
            )

@task
def bump(version=None):
    """ increment version """
    for app in apps:
        filename = os.path.join(app.location, "config.xml")
        with open(filename, "r") as filer:
            if not version:
                version = app.version
            version[-1] += 1
            content = filer.read()
            content = re.sub('version="(\d+.\d+.\d+)"',
                'version="%s"' % ".".join([str(a) for  a in version]), content)
            content = re.sub('versionCode="(\d+)"',
                'versionCode="%s"' % version[-1], content)

            with open(filename, "w") as filew:
                filew.write(content)

            app.save_version()

        print("%s version bumped to %s" % (app.name, ".".join([str(a) for  a in version])))


@task
def build():
    """ Compiles the mobile release package using local `cordova` library."""
    try:
        os.makedirs("dist/")
    except OSError:
        pass

    # to prevent false builds getting uploaded
    # we clean out previous builds
    with settings(warn_only=True):
        local("rm dist/*")

    execute("cordova.bump")
    execute("cordova.prepare")

    for app in apps:
        with lcd(app.location):

            ios_key = None
            if "ios" in app.platforms:
                if apps.debug:
                    ios_key = app.keys.get('ios')
                else:
                    ios_key = app.keys.get("ios-release", app.keys.get("ios"))

            if "ios" in app.platforms:
                filename = os.path.join(app.location, "platforms/ios/cordova/build.xcconfig")
                with open(filename, "w") as fileh:
                    fileh.write(
                        (
                            "PROVISIONING_PROFILE={profile}\n"
                            "CODE_SIGNING_IDENTITY={developer}\n"
                            "CODE_SIGN_RESOURCE_RULES_PATH=$(SDKROOT)/ResourceRules.plist\n"
                        ).format(
                            profile=ios_key.provision_uuid,
                            developer=not apps.debug and app.developer_release or app.developer
                        )
                    )

            if apps.debug:
                if "ios" in app.platforms:
                    local("cordova compile --device --universal")
                else:
                    local("cordova compile")
            else:
                if "ios" in app.platforms:
                    local("cordova compile --device --universal --release")
                else:
                    local("cordova compile --release")

        if apps.debug:
            if "android" in app.platforms:
                src = os.path.join(
                    app.location,
                    "platforms/android/ant-build/CordovaApp-debug.apk"
                )
                dest = "dist/%s.apk" % (app.name,)
                shutil.move(src, dest)

        else:
            if "android" in app.platforms:
                with lcd(os.path.join(app.location, "platforms/android/ant-build")):
                    key = app.keys["android"]
                    local("jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore %s -storepass %s -keypass %s *-release-unsigned.apk '%s'" %
                        (key.files["keystore"], key.password, key.password or key.keystore_password, key.alias))
                    local("jarsigner -verify -verbose -certs *-release-unsigned.apk")

                    release_apk = os.path.abspath("dist/%s.apk" % (app.name,))
                    local("zipalign -fv 4 *-release-unsigned.apk '%s'" % release_apk)

        if "ios" in app.platforms:

            # May need to run this...
            # local("security unlock-keychain")
            # local("open {profile-file-path}")

            provision = ios_key.files["profile.mobileprovision"]
            app_file = os.path.join(app.location, "platforms/ios/build/device/%s.app" % app.name)
            local(
                '/usr/bin/xcrun -sdk iphoneos PackageApplication -v "%s" '
                '-o "%s" --sign "%s" --embed "%s"' %
                (
                    app_file,
                    os.path.abspath("dist/%s.ipa" % app.name),
                    not apps.debug and app.developer_release or app.developer,
                    os.path.abspath(provision),
                )
            )

        print("{app} finished building {build}".format(
            app=app.name,
            build=app.build_code
        ))


@task
def run(app_slug=None):
    for app in apps:
        local("adb install -r 'dist/{name}.apk'".format(name=app.name))
#         local("adb shell am start -n com.package.name/com.package.name.ActivityName")


@task
def installr(notes="Automated Upload", notify=True):
    key = env.INSTALLR_KEY

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

    for app in apps:
        for platform in app.platforms:
            ext = (platform == "android" and ".apk" or ".ipa")
            response = requests.post(
                "https://www.installrapp.com/apps.json",
                headers={"X-InstallrAppToken": key},
                data=dict(
                    releaseNotes=notes,
                    build="2.0",
                    notify=notify and "true" or "false"
                ),
                files=dict(qqfile=open("dist/" + app.name + ext, "rb"))
            )

    for app in apps:
        print("{app} finished sending {build} to installr".format(
            app=app.name,
            build=app.build_code
        ))


@task
def google_play(message=None):
    for app in apps:
        for platform in app.platforms:
            if platform == "android":
                try:
                    pass
                except Exception, ex:
                    print()
                    print("error", type(ex), ex)
                    print()


@task
def testflight(message=None):
    for app in apps:
        for platform in app.platforms:
            if platform == "ios":
                try:
                    with open("dist/%s.%s" % (app.name, app.get_ext(platform))) as fileh:

                        if not message:
                            message = "automated upload"

                        print("testflight message", message)

                        data = {
                            "api_token": env.TESTFLIGHT_KEY,
                            "team_token": env.TESTFLIGHT_TEAM_KEY,
                            "notes": message,
                            "distribution_lists": "Suhba",
                            "replace": True
                        }

                        response = requests.post(
                            "http://testflightapp.com/api/builds.json",
                            data, files={"file": fileh})
                        print("response", response.text)

                        print(" # uploaded %s (%s)" % (app.name, platform))
                except Exception, ex:
                    print()
                    print("error", type(ex), ex)
                    print()


@task
def clean():
    execute("cordova.killstart")
    execute("cordova.prepare_images")


@task
def provision_uuid():
    for app in apps:
        if "ios-release" in app.keys:
            print(app.keys["ios-release"].provision_uuid)
        else:
            print(app.keys["ios"].provision_uuid)


@task
def install_keys():
    for app in apps:
        for platform, key in app.keys.items():
            if key.platform == "ios":
                local((
                    'uuid=`grep UUID -A1 -a {mp} | grep -o "[-A-Z0-9]\{{36\}}"` '
                    'cp {mp}  ~/Library/MobileDevice/Provisioning\ Profiles/$uuid.mobileprovision'
                ).format(mp=key.files['profile.mobileprovision']))
                local((
                    'security import {} '
                    '-k ~/Library/Keychains/login.keychain -P {} '
                    '-T /usr/bin/codesign -A'
                ).format(key.files['cert.p12'], key.password))


@task
def auto_java_home():
    local('''export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")''')

