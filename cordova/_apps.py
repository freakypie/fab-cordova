from os import path
from xml.dom import minidom
import base64
import json
import os
import re
import requests
import sys
import time

from fabric.api import hide
from fabric.context_managers import lcd
from fabric.operations import local
from fabric.state import env


class AppManager(object):
    apps = []
    debug = True

    _selected_apps = None

    def __init__(self):
        pass

    def add(self, *apps):
        self.apps.extend(apps)

    def __iter__(self):
        return [app for app in self.apps if app.slug in self.selected_apps].__iter__()

    def select(self, *apps):
        self._selected_apps = apps

    @property
    def selected_apps(self):
        if self._selected_apps:
            return self._selected_apps
        return [app.slug for app in self.apps]

apps = AppManager()


class PhoneGapBuildAccount(object):
    email = None
    password = None

    def __init__(self, email, password):
        self.email = email
        self.password = password

        if not self.get():
            self.set(self)

    def auth_headers(self):
        return {
            "Authorization": "Basic %s" % base64.encodestring(
                '%s:%s' % (self.email, self.password)).replace('\n', '')
        }

    @classmethod
    def get(cls):
        return env.get("phone-gap-account")

    @classmethod
    def set(cls, account):
        env["phone-gap-account"] = account


class Key(object):
    id = None
    platform = None
    title = None
    files = None
    password = None

    # android
    alias = None
    keystore_password = None

    _provision_uuid = None
    _cache = {}

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def check_id(self):

        # find key id if not given
        if not self.id:
            for key in Key.list():
                if key["platform"] == self.platform and key["title"] == self.title:
                    self.id = key['id']
                    break

        # if key doesn't exist, upload
        if not self.id:
            self.upload()

    @property
    def provision_uuid(self):
        if not self._provision_uuid:
            with open(self.files["profile.mobileprovision"], "r") as fileh:
                content = fileh.read()
                result = re.search("UUID<\/key>\W*?<string>([^<]+)<\/string>", content)
                if result and result.group(1):
                    self._provision_uuid = result.group(1)
                else:
                    print "couldn't find a uuid"
                    print result, result.groups
        return self._provision_uuid

    def upload(self, account=None):

        if not account:
            account = PhoneGapBuildAccount.get()

        # delete if it already exists
        for key in Key.list(account):
            if key["platform"] == self.platform and key.get("title") == self.title or self.id == key["id"]:
                self.delete(account)

        # upload
        if self.platform == "ios":
            data = dict(
                title=self.title,
                password=self.password
            )
            response = requests.post(
                "https://build.phonegap.com/api/v1/keys/ios",
                dict(data=json.dumps(data)),
                files=self.files,
                headers=account.auth_headers(),
                verify=False
            )
        elif self.platform == "android":
            data = dict(
                title=self.title,
                alias=self.alias,
                key_pw=self.password,
                keystore_pw=self.keystore_password
            )
            response = requests.post(
                "https://build.phonegap.com/api/v1/keys/android",
                dict(data=json.dumps(data)), files=self.files,
                headers=account.auth_headers())

        print "uploaded new key"
        print response.text

    def delete(self, account):
        if not account:
            account = PhoneGapBuildAccount.get()

        response = requests.delete("https://build.phonegap.com/api/v1/keys/%s/%s" %
            (self.platform, self.id), headers=account.auth_headers())
        assert response.status_code == 202
        print "deleted old key"

    def signing_data(self):
        if self.platform == "ios":
            return {
                "id": self.id,
                "password": self.password
            }
        elif self.platform == "android":
            return {
                "id": self.id,
                "key_pw": self.password,
                "keystore_pw": self.keystore_password or self.password
            }
        return {}

    @classmethod
    def list(cls, account=None):

        if not account:
            account = env["phone-gap-account"]

        if not cls._cache:
            response = requests.get('https://build.phonegap.com/api/v1/keys',
                headers=account.auth_headers(), verify=False)

            if not response.status_code == 200:
                raise Exception("Invalid Server Response:" + response.text)

            cls._cache = []
            for name, platform in response.json().get("keys", {}).items():
                for key in platform.get("all"):
                    key["platform"] = name
                    cls._cache.append(key)

        return cls._cache


def get_content(dom, tag):
    return dom.getElementsByTagName(tag)[0].firstChild.nodeValue.strip()


class CordovaApp(object):
    id = None
    slug = None
    keys = []
    account = None
    location = None
    developer = "iPhone Developer"
    developer_release = "iPhone Developer"

    _platforms = []
    _version = None
    dom = None

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        try:
            self.dom = minidom.parse(path.join(self.location, "config.xml"))
        except IOError:
            try:
                self.dom = minidom.parse(path.join(
                    self.location,
                    "configurations",
                    "config-sample.xml"
                ))
            except IOError:
                pass
        finally:
            if self.dom:
                self.name = get_content(self.dom, "name")
                self.description = get_content(self.dom, "description")

            try:
                with lcd(self.location):
                    self._populate_platforms()
                    self.build_code
            except IOError:
                pass

    @property
    def platforms(self):
        if not self._platforms:
            with lcd(self.location):
                self._populate_platforms()
        return self._platforms

    @property
    def version(self):
        if not self._version:
            self._version = self._populate_version()
        return self._version

    @property
    def build_code(self):
        if not self._version:
            self._version = self._populate_version()
        return self._version[-1]

    def _populate_version(self):
        if os.path.exists(".version"):
            filename = ".version"
        else:
            filename = os.path.join(self.location, "config.xml")
        with open(filename, "r") as filer:
            content = filer.read()
            if filename == ".version":
                code = content
            else:
                code = re.search('version="(\d+\.\d+\.\d+)"', content).group(1)

            if filename != ".version":
                self.save_version(code)

            version = [int(c) for c in code.split(".")]
        return version

    def save_version(self, code=None):
        with open(".version", "w") as version_file:
            code = ".".join([str(c) for c in self.version])
            version_file.write(code)

    def _populate_platforms(self):
        warn_only = env.warn_only or False
        env.warn_only = True
        platforms_path = \
            os.path.abspath(os.path.join(self.location, "platforms"))
        try:
            os.makedirs(platforms_path)
        except OSError:
            pass

        try:
            local("pwd")
            platforms = local("ls {}".format(platforms_path), capture=True)
            for platform in platforms.split():
                platform = platform.strip()
                if platform:
                    self._platforms.append(platform)
            self._platforms = set(self._platforms)
        except IndexError:
            pass
        env.warn_only = warn_only

    def pack(self, filename="site.zip"):
        """ Packs the site into a zipped file and returns the filename """
        files = path.join(self.location_folder, "*")
        with lcd(self.location):
            local("zip -r %s %s" % (filename, files))
            local("chmod 777 %s" % (filename,))

        return path.join(self.location, filename)

    def upload(self, account=None, platforms=None, debug=False):
        if not platforms:
            platforms = self._platforms

        data = {
            "keys": {},
            "platforms": platforms
        }

        for key in self.keys.values():
            if key in platforms and not debug:
                data['keys'][key.platform] = key.signing_data()

        files = {
            "file": open(self.pack(), "rb"),
        }

        response = requests.put(
            "https://build.phonegap.com/api/v1/apps/%s" % self.id,
            dict(data=json.dumps(data)),
            files=files,
            headers=account.auth_headers(),
            verify=False
        )
        print " - %s build uploaded to build.phonegap.com (platforms: %s)" % (
            self.name, ", ".join(platforms))
        retval = response.json()
        error = retval.get("error")
        if error:
            print response.text
            if error.get("blackberry") == "":
                print
                print "got crazy blackberry error."
                print "but we aren't even building for blackberry..."
                print
                print error
                print
            else:
                print "Error", error
                print "Aborting"
                print
                exit(1)

    def fetch(self, account, platforms=None):
        if not platforms:
            platforms = self._platforms

        try:
            os.makedirs("dist")
        except OSError:
            pass

        for platform in platforms:

            data = {}
            print " - Waiting for %s [%s] build..." % (self.name, platform)
            for attempt in range(10):
                response = requests.get(
                    "https://build.phonegap.com/api/v1/apps/%s/%s" % (
                        self.id,
                        platform
                    ),
                    headers=account.auth_headers(),
                    allow_redirects=False,
                    verify=False
                )

                if response.status_code == 302:
                    data = response.json()
                    if not "error" in data:
                        break  # done
                    else:
                        print data["error"]
                elif response.status_code == 404:
                    data = response.json()
                    seconds = min(120, attempt * 30)
                    print data['error']
                    print "error: %s, retry in %s seconds" % (data["error"], seconds)
                    time.sleep(seconds)
                else:
                    raise Exception("Error contacting build server[%s]: %s" % (
                        response.status_code, response.text))

            ext = self.get_ext(platform)
            if "location" in data:
                request = requests.head(data["location"])
                total_size = float(request.headers['content-length'])
                response = requests.get(data["location"], stream=True)

                lfile = open("dist/%s.%s" % (self.name, ext), "wb")

                size = 0
                for block in response.iter_content(1024):
                    if not block:
                        break

                    size += sys.getsizeof(block) - 37
                    print "\rdownload: %.0f%%" % ((size / total_size) * 100),
                    lfile.write(block)

                lfile.close()

                print "done"
            else:
                print "Couldn't download build... no location given. Phonegap build appears to be down"
                print response.status_code, data

        print "down downloading"


    @classmethod
    def get_ext(cls, platform):
        return {
            "ios": "ipa",
            "android": "apk",
        }[platform]
